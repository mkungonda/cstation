//
//  main.cpp
//  Hangman
//
//  Created by mac on 29.01.2017.
//  Copyright © 2017 mac. All rights reserved.
//

#include <iostream>
#include "master.hpp"

using namespace std;
int main(int argc, const char * argv[]) {
    
    master master;
    
    master.start();
    return 0;
}
