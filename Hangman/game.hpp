//
//  game.hpp
//  Hangman
//
//  Created by mac on 30.01.2017.
//  Copyright © 2017 mac. All rights reserved.
//

#ifndef game_hpp
#define game_hpp

#include <stdio.h>

class game{
public:
    game();
    virtual int start()     = 0;
    virtual int stop();
    virtual int restart()   = 0;
    virtual int nextStage() = 0;
    virtual ~game()          = default;
    virtual int rules()     = 0;
    static const int START      = 1;
    static const int CONTINUE   = 9;
    static const int RULES      = 2;
    static const int EXIT       = 3;
    static const int NEXT       = 4;
    static const int RESTART    = 5;
    static const int MENU       = 6;
};
#endif /* game_hpp */
