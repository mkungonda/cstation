//
//  player.cpp
//  Hangman
//
//  Created by mac on 31.01.2017.
//  Copyright © 2017 mac. All rights reserved.
//

#include <iostream>
#include "player.hpp"
#include "game.hpp"
#include "hangman.hpp"

using namespace std;

struct player::Private{
    static int menu(){
        int option;
        cout << "Game menu:" << endl;
        cout << "1 Start" << endl;
        cout << "2 Rules" << endl;
        cout << "3 Exit" << endl;
        cout << "---------------------" << endl;
        cout << "Where we go now: ";
        cin >> option;
        while (cin.fail() || !Private::processOption(option)){
            cout << "Oops, please try again: ";
            cin.clear();
            cin.ignore(256, '\n');
            cin >> option;
        }
        
        return option;
    }
    
    
    static bool processOption(int option){
        switch (option) {
            case START:
            case RULES:
            case EXIT:
                return true;
            default:
                return false;
        }
    }
    
    static void next(int option, game* game) {
        int nextAction = EXIT;
        switch (option) {
            case START:
                nextAction = game->start();
                break;
            case RULES:
                 nextAction = game->rules();
                break;
            case NEXT:
                nextAction = game->nextStage();
                break;
            case RESTART:
                nextAction = game->restart();
                break;
            case MENU:
                nextAction = menu();
                break;
            case EXIT:
            default:
                game->stop();
                return;
        }
        
        next(nextAction, game);
    }
};

player::player(){
    cout << "---------------------" << endl;
    cout << "Game is getting loaded" << endl;
    cout << "---------------------" << endl;
}

void player::play(game* game){
    Private::next(MENU, game);
}