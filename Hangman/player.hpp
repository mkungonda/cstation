//
//  player.hpp
//  Hangman
//
//  Created by mac on 31.01.2017.
//  Copyright © 2017 mac. All rights reserved.
//

#ifndef player_hpp
#define player_hpp

#include <stdio.h>
#include "game.hpp"

class player{
    public:
        player();
        void play(game* game);
    private:
        int option;
        struct Private;
        static const int START      = 1;
        static const int RULES      = 2;
        static const int EXIT       = 3;
        static const int NEXT       = 4;
        static const int RESTART    = 5;
        static const int MENU       = 6;
};
#endif /* player_hpp */
