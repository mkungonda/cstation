//
//  master.hpp
//  Hangman
//
//  Created by mac on 29.01.2017.
//  Copyright © 2017 mac. All rights reserved.
//

#ifndef master_hpp
#define master_hpp

#include <stdio.h>
#include <string>
#include <vector>

using namespace std;

class master
{
public:
    master();
    void start();
//    void nextAction();
private:
    struct Private;
    int selection;
    static const int HANGMAN            = 1;
    static const int ROCKPAPERSCISSORS  = 2;
    vector<string> games                = {
        "1 Hongman",
        "2 Rock-paper-scissors"
    };
//    void showMenu();
};
#endif /* master_hpp */
