//
//  master.cpp
//  Hangman
//
//  Created by mac on 29.01.2017.
//  Copyright © 2017 mac. All rights reserved.
//

#include "master.hpp"
#include <string>
#include <iostream>
#include <vector>
#include "hangman.hpp"
#include "rockpaperscissors.hpp"
#include "player.hpp"
#include "game.hpp"

using namespace std;

struct master::Private
{
    static bool validatePick(int param) {
        
        switch (param) {
            case HANGMAN:
            case ROCKPAPERSCISSORS:
                return true;
            default:
                return false;;
        }
    }
    
    static int pickGame() {
        int selection;
        
        cout << "Pass Game Number: ";
        cin >> selection;
        while (cin.fail() || !Private::validatePick(selection)) {
            cout << "Oops, please try again: ";
            cin.clear();
            cin.ignore(256, '\n');
            cin >> selection;
        }
        
        return selection;
    }
    
    static void process(int pick) {
        player* player = new class player;
 
        switch (pick) {
            case HANGMAN:
            {
                game* newGame = new class hangman;
                player->play(newGame);
                break;
            }
            case ROCKPAPERSCISSORS:
            {
                game* newGame = new class rockpaperscissors;
                player->play(newGame);
                break;
            }
            default:
                break;
        }
    }
};

master::master()
{
}

// implementation of "method"

void master::start() {
    cout << "Games List: " << endl;
    cout << "---------------------" << endl;
    for (vector<string>::iterator it = games.begin(); it != games.end(); ++it)
        cout << *it << endl;
    cout << "---------------------" << endl;
    Private::process(Private::pickGame());
    start();
}