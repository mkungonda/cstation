//
//  rockpaperscissors.cpp
//  Hangman
//
//  Created by mac on 29.01.2017.
//  Copyright © 2017 mac. All rights reserved.
//

#include "rockpaperscissors.hpp"
#include <string>
#include <iostream>
#include <vector>

using namespace std;

rockpaperscissors::~rockpaperscissors(){};

int rockpaperscissors::computerChoice (int min, int max){
    int n = max - min + 1;
    int remainder = RAND_MAX % n;
    int x;
    do{
        x = rand();
    }while (x >= RAND_MAX - remainder);
    return min + x % n;
}

bool rockpaperscissors::validatePick(int param, int group) {
    
    if(group == GAME_OPTIONS) {
        switch (param) {
            case ROCK:
            case PAPER:
            case SCISSORS:
                return true;
            default:
                return false;;
        }
    }
    if(group == MENU_OPTIONS){
        switch (param) {
            case CONTINUE:
            case RESTART:
            case EXIT:
                return true;
            default:
                return false;;
        }
    }
    return false;
};

int rockpaperscissors::processMenu(int selection){
    switch (selection) {
        case CONTINUE:
            return START;
        case RESTART:
            return RESTART;
        case EXIT:
        default:
            return EXIT;
            break;
    }
}

void rockpaperscissors::processResult(int selection) {
     int computerPick = computerChoice(1, 3);
    
    cout << "---------------------" << endl;
    cout << options.at(selection-1) << " vs " << options.at(computerPick-1) << endl;
   
    switch (selection) {
        case PAPER:
            if (computerPick == ROCK) {
                playerScore++;
            } else if (computerPick == SCISSORS){
                computerScore++;
            }
            break;
        case ROCK:
            if (computerPick == PAPER) {
                computerScore++;
            } else if (computerPick == SCISSORS) {
                playerScore++;
            }
            break;
        case SCISSORS:
            if (computerPick == ROCK) {
                computerScore++;
            } else if (computerPick == PAPER) {
                playerScore++;
            }
            break;
        default:
            break;
    }
}

void rockpaperscissors::showScore() {
    cout << "---------------------" << endl;
    cout << "Player " << playerScore << " - " << computerScore << " Computer " << endl;
}
    
void rockpaperscissors::updateScore(int playerScore, int computerScore) {
        
}
    
void rockpaperscissors::resetScore() {
    computerScore = 0;
    playerScore = 0;
}

int rockpaperscissors::rules(){
    cout << "---------------------" << endl;
    cout << "Rock-paper-scissors or Scissor-Paper-Rock, is a zero-sum hand game usually played between two people, in which each player simultaneously forms one of three shapes with an outstretched hand. These shapes are 'rock', 'paper', and 'scissors'. The game has only three possible outcomes other than a tie: a player who decides to play rock will beat another player who has chosen scissors ('rock crushes scissors') but will lose to one who has played paper ('paper covers rock'); a play of paper will lose to a play of scissors ('scissors cut paper'). If both players choose the same shape, the game is tied and is usually immediately replayed to break the tie. Other names for the game in the English-speaking world include roshambo and other orderings of the three items, sometimes with 'rock' being called 'stone'." << endl;
    cout << "---------------------" << endl;
    return MENU;
}

int rockpaperscissors::showMenu(){
    cout << "Pick one: " << endl;
    cout << "---------------------" << endl;
    for (vector<string>::iterator it = menu.begin(); it != menu.end(); ++it)
        cout << *it << endl;
    int selection;
    cin >> selection;
    while (cin.fail() || !validatePick(selection, MENU_OPTIONS)) {
        cout << "Oops, please try again: ";
        cin.clear();
        cin.ignore(256, '\n');
        cin >> selection;
    }
    
    return processMenu(selection);
}
int rockpaperscissors::nextStage(){
    showScore();
    return showMenu();
}

void rockpaperscissors::startRound(){
    cout << "Pick one: " << endl;
    cout << "---------------------" << endl;
    for (vector<string>::iterator it = options.begin(); it != options.end(); ++it)
        cout << *it << endl;
    cout << "---------------------" << endl;
    int selection;
    cin >> selection;
    while (cin.fail() || !validatePick(selection, GAME_OPTIONS)) {
        cout << "Oops, please try again: ";
        cin.clear();
        cin.ignore(256, '\n');
        cin >> selection;
    }
    
    processResult(selection);
}

int rockpaperscissors::start(){
    startRound();
    return NEXT;
}

int rockpaperscissors::restart(){
    resetScore();
    return START;
}