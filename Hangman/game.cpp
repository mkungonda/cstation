//
//  game.cpp
//  Hangman
//
//  Created by mac on 30.01.2017.
//  Copyright © 2017 mac. All rights reserved.
//

#include <iostream>
#include "game.hpp"

using namespace std;

game::game(){
    cout << "Game has been loaded!" << endl;
    cout << "---------------------" << endl;
}

int game::stop(){
    cout << "Game has been stopped!" << endl;
    cout << "---------------------" << endl;
    return EXIT;
};

