//
//  hangman.hpp
//  Hangman
//
//  Created by mac on 29.01.2017.
//  Copyright © 2017 mac. All rights reserved.
//

#ifndef hangman_hpp
#define hangman_hpp

#include <stdio.h>
#include <string>
#include "game.hpp"

using namespace std;

class hangman: public game
{
public:
    ~hangman();
    int start();
    int rules();
    int nextStage();
    int restart();
private:
    string word;
};
#endif /* hangman_hpp */
