//
//  rockpaperscissors.hpp
//  Hangman
//
//  Created by mac on 29.01.2017.
//  Copyright © 2017 mac. All rights reserved.
//

#ifndef rockpaperscissors_hpp
#define rockpaperscissors_hpp

#include <stdio.h>
#include <string>
#include <vector>
#include "game.hpp"

using namespace std;

class rockpaperscissors: public game
{
    public:
        ~rockpaperscissors();
        int start();
        int rules();
        int restart();
        int nextStage();
    private:
        static const int GAME_OPTIONS   = 10;
        static const int ROCK           = 1;
        static const int PAPER          = 2;
        static const int SCISSORS       = 3;
        vector<string> options          = {
            "1 Rock",
            "2 Paper",
            "3 Scissors",
        };
        static const int MENU_OPTIONS   = 41;
        vector<string> menu             = {
            "9 Continue",
            "5 Restart",
            "3 Exit",
        };
        int playerScore    = 0;
        int computerScore  = 0;
        void showScore();
        void updateScore(int p, int c);
        void resetScore();
        void startRound();
        bool validatePick(int p, int c);
        void processResult(int p);
        int computerChoice(int m, int n);
        int showMenu();
        int processMenu(int param);
};
#endif /* rockpaperscissors_hpp */
